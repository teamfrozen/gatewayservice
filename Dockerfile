FROM node:12-alpine

WORKDIR /app

RUN addgroup app &&\
    adduser -S app -G app &&\
    chown -R app /app

ADD . /app

EXPOSE 3000

ENTRYPOINT ["yarn", "start"]