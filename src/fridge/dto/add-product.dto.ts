import {ApiProperty} from '@nestjs/swagger';
import {IsDateString, IsNumber, IsOptional, IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {FridgeProduct} from '../product.entity';

export class AddProductBody {
  @ApiProperty()
  @IsString()
  productName!: string;

  @ApiProperty()
  @IsString()
  productTypeId!: string;

  @ApiProperty()
  @IsDateString()
  expirationDate!: string;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  quantity!: number;
}

export class AddProductResponse extends SuccessResponse {
  @ApiProperty({type: FridgeProduct, isArray: true})
  payload!: FridgeProduct[];
}
