import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {Product} from '../product.entity';

export class GetProductsByProductTypeIdParams {
  @ApiProperty()
  @IsString()
  productTypeId!: string;
}

export class GetProductsByProductTypeIdResponse extends SuccessResponse {
  @ApiProperty({type: Product, isArray: true})
  payload!: Product[];
}
