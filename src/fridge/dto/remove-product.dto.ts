import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {FridgeProduct} from '../product.entity';

export class RemoveProductBody {
  @ApiProperty()
  @IsString()
  productId!: string;
}

export class RemoveProductResponse extends SuccessResponse {
  @ApiProperty({type: FridgeProduct, isArray: true})
  payload!: FridgeProduct[];
}
