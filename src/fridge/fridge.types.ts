export interface CreateUserResponse {
  userId: string;
  fridgeId: string;
}

export interface CreateUserBody {
  userId: string;
  fridgeId?: null;
}

export interface AddProductToFridgeBody {
  productName: string;
  productTypeId: string;
  expirationDate: string;
  quantity: number;
}
