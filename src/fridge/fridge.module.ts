import {Module} from '@nestjs/common';
import {FridgeController} from './fridge.controller';
import {FirdgeService} from './fridge.service';

@Module({
  controllers: [FridgeController],
  providers: [FirdgeService],
  exports: [FirdgeService],
})
export class FridgeModule {}
