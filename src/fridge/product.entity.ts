import {ApiProperty} from '@nestjs/swagger';

export class ProductType {
  @ApiProperty()
  productTypeId!: string;

  @ApiProperty()
  productTypeName!: string;
}

export class Product {
  @ApiProperty()
  productId!: string;

  @ApiProperty()
  productName!: string;
}

export class FridgeProduct {
  @ApiProperty()
  productInFridgeId!: string;

  @ApiProperty()
  depositDate!: string;

  @ApiProperty()
  expirationDate!: string;

  @ApiProperty()
  productName!: string;

  @ApiProperty()
  productId!: string;

  @ApiProperty()
  quantity!: number;

  @ApiProperty()
  productTypeId!: string;

  @ApiProperty()
  productTypeName!: string;

  @ApiProperty()
  productImagePath!: string;

  @ApiProperty()
  measureName!: string;

  @ApiProperty()
  isFavorite!: boolean;
}
