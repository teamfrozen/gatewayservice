export enum FridgeRoute {
  CreateUser = '/Users',
  AddProductToFridge = '/Fridges/OwnerContents/:userId/AddProductManually',
  GetProductsFromFridge = '/Fridges/OwnerContents/:userId',
  GetProducts = '/Products',
  GetProductTypes = '/ProductTypes',
  RemoveProductFromFridge = '/Fridges/OwnerContents/:userId/:productInFridgeId',
  GetProductsByProductTypeId = '/Products/OfType/:productTypeId',
}

export enum FridgeControllerRoute {
  Root = '/api/v1',
  AddProductToFridge = '/fridge',
  RemoveProductFromFridge = '/fridge',
  GetProductsByProductTypeId = '/fridge/products/productType/:productTypeId',
}
