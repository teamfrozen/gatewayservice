import {Body, Controller, Delete, Get, Param, Post, Req, UseGuards} from '@nestjs/common';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger';
import {RequestWithUser} from '../auth/auth.types';
import {UserGuard} from '../auth/strategies/user/user.guard';
import {AddProductBody, AddProductResponse} from './dto/add-product.dto';
import {
  GetProductsByProductTypeIdParams,
  GetProductsByProductTypeIdResponse,
} from './dto/get-products-by-product-type-id.dto';
import {RemoveProductBody, RemoveProductResponse} from './dto/remove-product.dto';
import {FridgeControllerRoute} from './fridge.routes';
import {FirdgeService} from './fridge.service';

@ApiTags('Fridge')
@Controller(FridgeControllerRoute.Root)
export class FridgeController {
  constructor(private readonly fridgeService: FirdgeService) {}

  @Get(FridgeControllerRoute.GetProductsByProductTypeId)
  @ApiBearerAuth()
  @UseGuards(UserGuard)
  public async getProductsByProductTypeId(
    @Param() {productTypeId}: GetProductsByProductTypeIdParams,
  ): Promise<GetProductsByProductTypeIdResponse> {
    const payload = await this.fridgeService.getProductsByProductTypeId(productTypeId);

    return {status: 'ok', payload};
  }

  @Post(FridgeControllerRoute.AddProductToFridge)
  @ApiBearerAuth()
  @UseGuards(UserGuard)
  public async addProductToFridge(
    @Req() {user}: RequestWithUser,
    @Body() data: AddProductBody,
  ): Promise<AddProductResponse> {
    await this.fridgeService.addProductToFridge(user.id, data);

    const payload = await this.fridgeService.getProductsFromFridge(user.id);

    return {status: 'ok', payload};
  }

  @Delete(FridgeControllerRoute.RemoveProductFromFridge)
  @ApiBearerAuth()
  @UseGuards(UserGuard)
  public async removeProductFromFridge(
    @Req() {user}: RequestWithUser,
    @Body() {productId}: RemoveProductBody,
  ): Promise<RemoveProductResponse> {
    await this.fridgeService.removeProductFromFridge(user.id, productId);

    const payload = await this.fridgeService.getProductsFromFridge(user.id);

    return {status: 'ok', payload};
  }
}
