import {Injectable} from '@nestjs/common';
import {AppConfig} from '../config/app-config';
import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import {AddProductToFridgeBody, CreateUserBody, CreateUserResponse} from './fridge.types';
import {replaceParam} from '../utils/string';
import {FridgeRoute} from './fridge.routes';
import {FridgeProduct, Product, ProductType} from './product.entity';

@Injectable()
export class FirdgeService {
  private api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: AppConfig.APP_FRIDGE_SERVICE_URL,
    });
  }

  public async getProductsByProductTypeId(productTypeId: string): Promise<Product[]> {
    return await this.api.request(this.buildGetProductsByProductTypeIdParams(productTypeId)).then(this.extractData);
  }

  private buildGetProductsByProductTypeIdParams(productTypeId: string): AxiosRequestConfig {
    return {
      method: 'GET',
      url: replaceParam(FridgeRoute.GetProductsByProductTypeId, productTypeId),
    };
  }

  public async getProductTypes(): Promise<ProductType[]> {
    return await this.api.request(this.buildGetProductTypeListParams()).then(this.extractData);
  }

  private buildGetProductTypeListParams(): AxiosRequestConfig {
    return {
      method: 'GET',
      url: FridgeRoute.GetProductTypes,
    };
  }

  public async removeProductFromFridge(userId: string, productId: string): Promise<void> {
    await this.api.request(this.buildRemoveProductFromFridge(userId, productId));
  }

  private buildRemoveProductFromFridge(userId: string, productId: string): AxiosRequestConfig {
    return {
      url: replaceParam(replaceParam(FridgeRoute.RemoveProductFromFridge, userId), productId),
      method: 'DELETE',
    };
  }

  public async getProducts(): Promise<Product[]> {
    return this.api.request<Product[]>(this.buildGetProducts()).then(this.extractData);
  }

  private buildGetProducts(): AxiosRequestConfig {
    return {
      method: 'GET',
      url: FridgeRoute.GetProducts,
    };
  }

  public async getProductsFromFridge(userId: string): Promise<FridgeProduct[]> {
    return this.api.request<FridgeProduct[]>(this.buildGetProductsFromFridgeParams(userId)).then(this.extractData);
  }

  private buildGetProductsFromFridgeParams(userId: string): AxiosRequestConfig {
    return {
      method: 'GET',
      url: replaceParam(FridgeRoute.GetProductsFromFridge, userId),
    };
  }

  public async addProductToFridge(userId: string, data: AddProductToFridgeBody) {
    return this.api
      .request<CreateUserResponse>(this.buildAddProductToFridgeParams(userId, data))
      .then(this.extractData);
  }

  private buildAddProductToFridgeParams(userId: string, data: AddProductToFridgeBody): AxiosRequestConfig {
    return {
      method: 'POST',
      url: replaceParam(FridgeRoute.AddProductToFridge, userId),
      data: {
        ...data,
        productTypeId: null,
      },
    };
  }

  public async createUser(data: CreateUserBody): Promise<CreateUserResponse> {
    return this.api.request<CreateUserResponse>(this.buildCreateUserParams(data)).then(this.extractData);
  }

  private buildCreateUserParams(data: CreateUserBody): AxiosRequestConfig {
    return {
      method: 'POST',
      url: FridgeRoute.CreateUser,
      data,
    };
  }

  private extractData<T>(response: AxiosResponse<T>): T {
    return response.data;
  }
}
