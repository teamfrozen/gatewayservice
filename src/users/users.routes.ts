export enum UsersServiceRoute {
  SignUserByTelegram = '/auth/sign/telegram',
  SignUserByCredentials = '/auth/sign',
  AuthUserByCredentials = '/auth/login',
  GetUsersList = '/users',
  CreateUserOrIgnore = '/users',
  GetUserByIdOrFail = '/users/:id',
  UpdateUserOrFail = '/users/:id',
  RemoveUserOrFail = '/users/:id',
  GetUserByTelegramIdOrFail = '/users/telegram/:telegramId',
}
