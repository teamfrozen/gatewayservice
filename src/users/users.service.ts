import {Injectable} from '@nestjs/common';
import axios, {AxiosInstance, AxiosResponse, AxiosRequestConfig} from 'axios';
import {AppConfig} from '../config/app-config';
import {replaceParam} from '../utils/string';
import {User} from './user.entity';
import {UsersServiceRoute} from './users.routes';

interface ResponseWithPayload<T> {
  status: 'ok';
  payload: T;
}

@Injectable()
export class UsersService {
  private api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: AppConfig.APP_USERS_SERVICE_URL,
      headers: {
        ApiKey: AppConfig.APP_USERS_SERVICE_API_KEY,
      },
    });
  }

  public async getUserByTelegramId(telegramId: string): Promise<User | undefined> {
    return this.api
      .request<ResponseWithPayload<User>>(this.buildGetUserByTelegramIdParams(telegramId))
      .then(this.extractPayload);
  }

  private buildGetUserByTelegramIdParams(telegramId: string): AxiosRequestConfig {
    return {
      url: replaceParam(UsersServiceRoute.GetUserByTelegramIdOrFail, telegramId),
      method: 'GET',
    };
  }

  public async signUserByTelegramId(telegramId: string, firstName: string): Promise<User> {
    return this.api
      .request<ResponseWithPayload<User>>(this.buildSignUserByTelegramId({telegramId, firstName}))
      .then(this.extractPayload);
  }

  private buildSignUserByTelegramId(data: Partial<User>): AxiosRequestConfig {
    return {
      url: UsersServiceRoute.SignUserByTelegram,
      method: 'POST',
      data,
    };
  }

  public getUserByIdOrFail(id: string): Promise<User> {
    return this.api.request<ResponseWithPayload<User>>(this.buildGetUserByIdOrFail(id)).then(this.extractPayload);
  }

  private buildGetUserByIdOrFail(id: string): AxiosRequestConfig {
    return {
      url: replaceParam(UsersServiceRoute.GetUserByIdOrFail, id),
      method: 'GET',
    };
  }

  public async signUserByCredentials(user: Partial<User & {password: string}>): Promise<User> {
    return this.api.request<ResponseWithPayload<User>>(this.buildSignUserByCredentials(user)).then(this.extractPayload);
  }

  private buildSignUserByCredentials(data: Partial<User>): AxiosRequestConfig {
    return {
      url: UsersServiceRoute.SignUserByCredentials,
      method: 'POST',
      data,
    };
  }

  public async authUserByCredentials(login: string, password: string): Promise<User> {
    return this.api
      .request<ResponseWithPayload<User>>(this.buildAuthUserByCredentials(login, password))
      .then(this.extractPayload);
  }

  private buildAuthUserByCredentials(login: string, password: string): AxiosRequestConfig {
    return {
      url: UsersServiceRoute.AuthUserByCredentials,
      method: 'POST',
      data: {login, password},
    };
  }

  private extractPayload<T>(response: AxiosResponse<{payload: T}>): T {
    return response.data.payload;
  }
}
