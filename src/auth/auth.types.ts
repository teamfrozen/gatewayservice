import {User} from '../users/user.entity';
import {Request} from 'express';

export type RequestWithUser = Request & {user: User};
