import {Injectable, UnauthorizedException} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {FirdgeService} from '../fridge/fridge.service';
import {FridgeProduct, ProductType} from '../fridge/product.entity';
import {User} from '../users/user.entity';
import {UsersService} from '../users/users.service';
import {AuthorizedUser} from './dto/authorized-user.dto';
import noop from 'lodash/noop';
import {RecommendationsService} from '../recommendations/recommendations.service';
import {Recommendation} from '../recommendations/recommendation.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly recommendationsService: RecommendationsService,
    private readonly usersService: UsersService,
    private readonly fridgeService: FirdgeService,
    private readonly jwtService: JwtService,
  ) {}

  public async signUserByCredentials(login: string, password: string, firstName: string): Promise<AuthorizedUser> {
    const user = await this.usersService
      .signUserByCredentials({login, password, firstName})
      .catch(this.captureException);

    await this.fridgeService.createUser({userId: user.id});

    const productTypes = await this.fridgeService.getProductTypes();
    const recommendations = await this.recommendationsService.getUserRecommendations(user.id);

    return this.mapToAuthorizedUser(user, [], productTypes, recommendations);
  }

  public async signUserByTelegram(telegramId: string, firstName: string): Promise<AuthorizedUser> {
    const user = await this.usersService.getUserByTelegramId(telegramId).catch(noop);
    if (user) {
      return await this.loginByTelegramId(user);
    } else {
      return await this.authByTelegramId(telegramId, firstName);
    }
  }

  private async loginByTelegramId(user: User): Promise<AuthorizedUser> {
    const fridge = await this.fridgeService.getProductsFromFridge(user.id);
    const productTypes = await this.fridgeService.getProductTypes();
    const recommendations = await this.recommendationsService.getUserRecommendations(user.id);

    return this.mapToAuthorizedUser(user, fridge, productTypes, recommendations);
  }

  private async authByTelegramId(telegramId: string, firstName: string): Promise<AuthorizedUser> {
    const user = await this.usersService.signUserByTelegramId(telegramId, firstName).catch(this.captureException);

    await this.fridgeService.createUser({userId: user.id});

    const productTypes = await this.fridgeService.getProductTypes();
    const recommendations = await this.recommendationsService.getUserRecommendations(user.id);

    return this.mapToAuthorizedUser(user, [], productTypes, recommendations);
  }

  public async authUserByCredentials(login: string, password: string): Promise<AuthorizedUser> {
    const user = await this.usersService.authUserByCredentials(login, password).catch(this.captureException);

    const fridge = await this.fridgeService.getProductsFromFridge(user.id);
    const productTypes = await this.fridgeService.getProductTypes();
    const recommendations = await this.recommendationsService.getUserRecommendations(user.id);

    return this.mapToAuthorizedUser(user, fridge, productTypes, recommendations);
  }

  public async authUserByToken(user: User): Promise<AuthorizedUser> {
    const fridge = await this.fridgeService.getProductsFromFridge(user.id);
    const productTypes = await this.fridgeService.getProductTypes();
    const recommendations = await this.recommendationsService.getUserRecommendations(user.id);

    return this.mapToAuthorizedUser(user, fridge, productTypes, recommendations);
  }

  public mapToAuthorizedUser(
    user: User,
    fridge: FridgeProduct[],
    productTypes: ProductType[],
    recommendations: Recommendation[],
  ): AuthorizedUser {
    return {
      token: this.createUserToken(user),
      data: {
        id: user.id,
        firstName: user.firstName,
        login: user.login,
        telegramId: user.telegramId,
        fridge,
        products: [],
        productTypes,
        recommendations,
      },
    };
  }

  private createUserToken(user: User): string {
    return this.jwtService.sign({id: user.id});
  }

  private captureException(error: Error): never {
    console.log(error);
    throw new UnauthorizedException();
  }
}
