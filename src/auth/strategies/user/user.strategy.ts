import {Injectable, UnauthorizedException} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import {ExtractJwt, Strategy} from 'passport-jwt';
import {AppConfig} from '../../../config/app-config';
import {UsersService} from '../../../users/users.service';

import {AuthStrategy} from '../../auth.constants';

@Injectable()
export class UserAuthStrategy extends PassportStrategy(Strategy, AuthStrategy.User) {
  constructor(private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: AppConfig.APP_SECRET,
    });
  }

  async validate({id}: {id: string}) {
    if (!id) {
      throw new UnauthorizedException();
    }

    try {
      return await this.usersService.getUserByIdOrFail(id);
    } catch {
      throw new UnauthorizedException();
    }
  }
}
