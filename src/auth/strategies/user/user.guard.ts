import {Injectable} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';

import {AuthStrategy} from '../../auth.constants';

@Injectable()
export class UserGuard extends AuthGuard(AuthStrategy.User) {}
