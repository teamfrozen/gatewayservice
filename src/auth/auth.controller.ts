import {Body, Controller, Get, Post, Req, UseGuards} from '@nestjs/common';
import {ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags} from '@nestjs/swagger';
import {AuthControllerRoutes} from './auth.routes';
import {AuthService} from './auth.service';
import {RequestWithUser} from './auth.types';
import {AuthUserByCredentialsBody, AuthUserByCredentialsResponse} from './dto/auth-user-by-credentials.dto';
import {AuthUserByTokenResponse} from './dto/auth-user-by-token.dto';
import {SignUserByCredentialsBody, SignUserByCredentialsResponse} from './dto/sign-user-by-credentials.dto';
import {SignUserByTelegramBody, SignUserByTelegramResponse} from './dto/sign-user-by-telegram.dto';
import {UserGuard} from './strategies/user/user.guard';

@ApiTags('Auth')
@Controller(AuthControllerRoutes.Root)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get(AuthControllerRoutes.AuthUserByToken)
  @ApiBearerAuth()
  @UseGuards(UserGuard)
  @ApiOkResponse({type: AuthUserByTokenResponse})
  @ApiOperation({
    operationId: 'authUserByToken',
    summary: 'Authorize user by token',
  })
  public async authUserByToken(@Req() {user}: RequestWithUser): Promise<AuthUserByTokenResponse> {
    const payload = await this.authService.authUserByToken(user);

    return {status: 'ok', payload};
  }

  @Post(AuthControllerRoutes.AuthUserByCredentials)
  @ApiOkResponse({type: AuthUserByCredentialsResponse})
  @ApiOperation({
    operationId: 'authUserByCredentials',
    summary: 'Authorize user by credentials',
  })
  public async authUserByCredentials(
    @Body() {login, password}: AuthUserByCredentialsBody,
  ): Promise<AuthUserByCredentialsResponse> {
    const payload = await this.authService.authUserByCredentials(login, password);

    return {status: 'ok', payload};
  }

  @Post(AuthControllerRoutes.SignUserByTelegram)
  @ApiOkResponse({type: SignUserByTelegramResponse})
  @ApiOperation({
    operationId: 'signUserByTelegram',
    summary: 'Sign user by telegram',
  })
  public async signUserByTelegram(
    @Body() {telegramId, firstName}: SignUserByTelegramBody,
  ): Promise<SignUserByTelegramResponse> {
    const payload = await this.authService.signUserByTelegram(telegramId, firstName);

    return {status: 'ok', payload};
  }

  @Post(AuthControllerRoutes.SignUserByCredentials)
  @ApiOkResponse({type: SignUserByCredentialsResponse})
  @ApiOperation({
    operationId: 'signUserByCredentials',
    summary: 'Sign user by credentials',
  })
  public async signUserByCredentials(
    @Body() {login, firstName, password}: SignUserByCredentialsBody,
  ): Promise<AuthUserByCredentialsResponse> {
    const payload = await this.authService.signUserByCredentials(login, password, firstName);

    return {status: 'ok', payload};
  }
}
