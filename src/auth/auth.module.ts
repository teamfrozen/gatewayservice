import {Module} from '@nestjs/common';
import {JwtModule} from '@nestjs/jwt';
import {AppConfig} from '../config/app-config';
import {FridgeModule} from '../fridge/fridge.module';
import {RecommendationsModule} from '../recommendations/recommendations.module';
import {UsersModule} from '../users/users.module';
import {AuthController} from './auth.controller';
import {AuthService} from './auth.service';
import {ApiKeyStrategy} from './strategies/api-key/api-key.strategy';
import {UserAuthStrategy} from './strategies/user/user.strategy';

@Module({
  imports: [JwtModule.register({secret: AppConfig.APP_SECRET}), UsersModule, FridgeModule, RecommendationsModule],
  providers: [AuthService, ApiKeyStrategy, UserAuthStrategy],
  exports: [AuthService, ApiKeyStrategy, UserAuthStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
