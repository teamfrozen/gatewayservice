import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {AuthorizedUser} from './authorized-user.dto';

export class SignUserByCredentialsBody {
  @ApiProperty()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsString()
  firstName!: string;

  @ApiProperty()
  @IsString()
  password!: string;
}

export class SignUserByCredentialsResponse extends SuccessResponse {
  @ApiProperty({type: AuthorizedUser})
  payload!: AuthorizedUser;
}
