import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {AuthorizedUser} from './authorized-user.dto';

export class SignUserByTelegramBody {
  @ApiProperty()
  @IsString()
  telegramId!: string;

  @ApiProperty()
  @IsString()
  firstName!: string;
}

export class SignUserByTelegramResponse extends SuccessResponse {
  @ApiProperty({type: AuthorizedUser})
  payload!: AuthorizedUser;
}
