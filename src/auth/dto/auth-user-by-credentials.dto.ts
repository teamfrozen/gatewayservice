import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {AuthorizedUser} from './authorized-user.dto';

export class AuthUserByCredentialsResponse extends SuccessResponse {
  @ApiProperty({type: AuthorizedUser})
  payload!: AuthorizedUser;
}

export class AuthUserByCredentialsBody {
  @ApiProperty()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsString()
  password!: string;
}
