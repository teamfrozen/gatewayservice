import {ApiProperty} from '@nestjs/swagger';
import {Product, FridgeProduct, ProductType} from '../../fridge/product.entity';
import {Recommendation} from '../../recommendations/recommendation.entity';

export class AuthorizedUserData {
  @ApiProperty()
  id?: string;

  @ApiProperty()
  firstName?: string;

  @ApiProperty()
  login?: string;

  @ApiProperty()
  telegramId?: string;

  @ApiProperty({type: FridgeProduct, isArray: true})
  fridge!: FridgeProduct[];

  @ApiProperty({type: Product, isArray: true})
  products!: Product[];

  @ApiProperty({type: ProductType, isArray: true})
  productTypes!: ProductType[];

  @ApiProperty({type: Recommendation, isArray: true})
  recommendations!: Recommendation[];
}

export class AuthorizedUser {
  @ApiProperty()
  token!: string;

  @ApiProperty({type: AuthorizedUserData})
  data!: AuthorizedUserData;
}
