import {ApiProperty} from '@nestjs/swagger';
import {SuccessResponse} from '../../common/response-success';
import {AuthorizedUser} from './authorized-user.dto';

export class AuthUserByTokenResponse extends SuccessResponse {
  @ApiProperty({type: AuthorizedUser})
  payload!: AuthorizedUser;
}
