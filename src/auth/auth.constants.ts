export enum AuthStrategy {
  User = 'user',
  ApiKey = 'apiKey',
}
