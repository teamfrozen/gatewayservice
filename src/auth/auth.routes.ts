export enum AuthControllerRoutes {
  Root = '/api/v1/auth',
  AuthUserByToken = '/me',
  SignUserByCredentials = '/sign',
  SignUserByTelegram = '/sign/telegram',
  AuthUserByCredentials = '/login',
}
