import {Controller, Get} from '@nestjs/common';
import {ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {StatusResponse} from './dto/status.dto';

@ApiTags('Status')
@Controller()
export class StatusController {
  @Get('/status')
  @ApiOkResponse({type: StatusResponse})
  public status(): StatusResponse {
    return {status: 'ok'};
  }
}
