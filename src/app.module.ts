import {Module} from '@nestjs/common';
import {StatusModule} from './status/status.module';
import {LoggerModule} from './logger/logger.module';
import {AuthModule} from './auth/auth.module';
import {UsersModule} from './users/users.module';
import {FridgeModule} from './fridge/fridge.module';
import {RecommendationsModule} from './recommendations/recommendations.module';

@Module({
  imports: [
    // prettier-ignore
    RecommendationsModule,
    LoggerModule,
    AuthModule,
    UsersModule,
    FridgeModule,
    StatusModule,
  ],
})
export class AppModule {}
