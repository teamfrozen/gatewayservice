import {INestApplication} from '@nestjs/common';

export async function teardown(app: INestApplication | undefined): Promise<void> {
  if (app) {
    await app.close();
  }
}
