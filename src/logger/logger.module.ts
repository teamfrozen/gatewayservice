import {Module} from '@nestjs/common';
import {LoggerModule as NestPinoLoggerModule} from 'nestjs-pino';

@Module({
  imports: [
    NestPinoLoggerModule.forRoot({
      // @See https://github.com/iamolegga/nestjs-pino#synchronous-configuration
      pinoHttp: {prettyPrint: true, autoLogging: process.env.NODE_ENV !== 'test'},
    }),
  ],
})
export class LoggerModule {}
