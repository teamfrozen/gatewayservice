import {ApiProperty} from '@nestjs/swagger';

export class Recommendation {
  @ApiProperty()
  name!: string;

  @ApiProperty()
  difficulty!: string;

  @ApiProperty()
  hours!: number;

  @ApiProperty()
  minutes!: number;

  @ApiProperty()
  products!: string[];

  @ApiProperty()
  steps!: string[];

  @ApiProperty()
  imagePath!: string;

  @ApiProperty()
  recipe_id!: string;
}
