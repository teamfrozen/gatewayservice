export enum RecommendationRoute {
  GetRecommendationsByUserId = '/recipes/userListRecipes/:userId',
}
