import {Injectable} from '@nestjs/common';
import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import {AppConfig} from '../config/app-config';
import {replaceParam} from '../utils/string';
import {Recommendation} from './recommendation.entity';
import {RecommendationRoute} from './recommendations.routes';

@Injectable()
export class RecommendationsService {
  private api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: AppConfig.APP_RECOMMENDATIONS_SERVICE_URL,
    });
  }

  public async getUserRecommendations(userId: string): Promise<Recommendation[]> {
    return this.api.request<Recommendation[]>(this.buildGetUserRecommendationsParams(userId)).then(this.extractData);
  }

  private buildGetUserRecommendationsParams(userId: string): AxiosRequestConfig {
    return {
      method: 'GET',
      url: replaceParam(RecommendationRoute.GetRecommendationsByUserId, userId),
    };
  }

  private extractData<T>(response: AxiosResponse<T>): T {
    return response.data;
  }
}
