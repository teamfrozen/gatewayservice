import {ApiProperty} from '@nestjs/swagger';

export class SuccessResponse {
  @ApiProperty({example: 'ok'})
  status!: 'ok';
}
