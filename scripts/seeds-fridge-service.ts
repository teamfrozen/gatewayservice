import axios from 'axios';

const products = ['Milk', 'Cheese', 'Eggs', 'Cream cheese', 'Cucumbers',
    'Tomatoes', 'Soda', 'Peant butter', 'Strawberry Jelly',
    'Bluberry Jelly', 'Bread', 'Butter', 'Cheetos', 'Lays',
    'Milky Way', 'CosmoStars', 'Pizza', 'Chicken', 'Fish',
    'Meat', 'Grapes', 'Pantry', 'Ceral', 'Canned soup',
    'Canned beans', 'Pasta', 'Garlic', 'Bannanas', 'Avacadoes',
    'Ramen', 'Cooking spray', 'Yogurt', 'cinnamon', 'nutmeg', 'eggs', 'milk', 'vanilla extract', ' white bread', 'maple syrup'];
    


main().catch(console.log);

export async function main() {


    for (const productName of products) {
        // await axios({
        //     url: 'http://localhost:8001/api/Products',
        //     method: 'POST',
        //     data: {
        //         "productName": productName,
        //         "quantity": 0,
        //         "shelfLifeDuration": {
        //             "days": 2,
        //             "hours": 0
        //         },
        //         "productTypeId": null
        //     }
        // }).catch(console.log);

        // await axios({
        //     url: 'http://localhost:8001/api/ProductTypes',
        //     method: 'POST',
        //     data:
        //         {
        //             "productTypeName": productName,
        //             "productAliases": [
        //                 productName
        //             ],
        //             "averageShelfLifeDuration": {
        //               "days": 0,
        //               "hours": 0
        //             },
        //             "measureName": productName,
        //         }
        // }).catch(console.log);
    }

    const userId = "3fa85f64-5717-4562-b3fc-2c963f66afa6";

        // await axios({
        //     url: 'http://localhost:8001/api/Users',
        //     method: 'POST',
        //     data: {
        //         "userId": userId,
        //         "fridgeId": null
        //       }
        // }).catch(console.log);


        const products2 = await axios({
            url: `http://localhost:8001/api/ProductTypes`,
        }).then(res => res.data).catch(console.log);
        

        for (let i = 0;i < products2.length; i++) {
            await axios({
                url: `http://localhost:8001/api/Fridges/OwnerContents/${userId}/AddProductManually`,
                method: 'POST',
                data: {
                    "productName": products[i],
                    "productTypeId": products2[i].productTypeId,
                    "expirationDate": "2022-12-16T07:39:03.210Z",
                    "quantity": 0
                  }
            }).catch(console.log);
        }
}