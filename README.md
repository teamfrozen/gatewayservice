# Gateway Service

## Getting started

```bash
# start service
$ docker-compose up
```

## Swagger

Swagger is enabled on `localhost:3001/docs`
